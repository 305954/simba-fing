const Scholarship = require('../models/scholarship');

const createScholarship=((req,res,next)=>{
    let scholarship=new Scholarship({
        key: req.body.key,
        title: req.body.title,
        description: req.body.description,
        credits: req.body.credits
    });

    Scholarship.find({
        $or:[
            {key:scholarship.key},
            {title:scholarship.title}
        ]
    }).exec((err,found)=>{
        if(err){
            return res.status(500).json({
                errors:[{message:"An unexpected error has ocurred"}],
                data:[]
            });
        }
        if(found && found.length>0){
            return res.status(302).json({
                errors:[{message:"The title is already on use, please try a different one"}],
                data:[]
            });
        }else{
            scholarship.save()
                .then((std)=>{
                    res.status(200).json({
                        errors:[],
                        data:std
                    });
                })
                .catch((err)=>{
                    res.status(500).json({
                        errors:[{message:"The scholarship couldn't be saved"}],
                        data:[]
                    });
                })
        }
    })
});

const viewAll=((req,res)=>{
    const options={
        page:1,
        limit:100,
        select : 'key title description credits'
    };
    Scholarship.paginate({},options)
        .then((scholarships) => {
            res.status(200).json({
                errors:[],
                //   data:scholarships,
                data:scholarships
            });
        }).catch((err) => {
        res.status(500).json({
            errors:[{message:"The data couldn't be retrieved"}],
            data:[]
        });
    });
});

const findScholarship=((req,res)=>{
    Scholarship.findById(req.params.id)
        .then((std)=>{
            res.status(200).json({
                errors:[],
                data:std
            });
        })
        .catch((err)=>{
            res.status(500).json({
                errors:[{message:"An error has ocurred"}],
                data:[]
            });
        });
});

const updateScholarship=((req,res)=>{
    Scholarship.findById(req.params.id)
        .then((std)=>{
            std.key= req.body.key ? req.body.key :std.key;
            std.title= req.body.title ? req.body.title :std.title;
            std.description= req.body.description ? req.body.description :std.description;
            std.credits=req.body.credits ? req.body.credits : std.credits;


            std.save()
                .then((std)=>{
                    res.status(200).json({
                        errors:[],
                        data:std
                    });
                }).catch(()=>{
                res.status(500).json({
                    errors:[{message:"An error ocurred while updating the scholarship"}],
                    data:[]
                });
            });
        })
        .catch((err)=>{
            res.status(500).json({
                errors:[{message:"An error ocurred while updating the scholarship"}],
                data:[]
            });
        });
});

const deleteScholarship = ((req,res)=>{
    Scholarship.deleteOne({_id:req.params.id})
        .then((std)=>{
            res.status(200).json({
                errors:[],
                data:std
            });
        })
        .catch((err)=>{
            res.status(500).json({
                errors:[{message:"An error ocurred while deleting the scholarship"}],
                data:[]
            });
        });
});

module.exports={
    viewAll,createScholarship,findScholarship,updateScholarship,deleteScholarship
};