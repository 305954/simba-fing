const User = require('../models/user');
const Student =require('../models/student');

const bcrypt = require('bcrypt-nodejs');
const jwt2=require('jsonwebtoken');


const createUser=((req,res,next)=>{
    let user=new User({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        email: req.body.email,
        // password: req.body.password,
        password: '12345',
        user_type: req.body.user_type
    });

    User.find({
        $or:[
            {email:user.email}
        ]
    }).exec((err,found)=>{
        if(err){
            return res.status(500).json({
                errors:[{message:"An unexpected error has ocurred"}],
                data:[]
            });
        }
        if(found && found.length>0){
            return res.status(302).json({
                errors:[{message:"The email is already on use, please try a different one"}],
                data:[]
            });
        }else{
            bcrypt.hash(user.password, null, null, (err, hash)=>{
                user.password = hash;
                user.save()
                .then((std)=>{
                    res.status(200).json({
                        errors:[],
                        data:std
                    });
                })
                .catch((err)=>{
                    res.status(500).json({
                        errors:[{message:"The user couldn't be saved"}],
                        data:[]
                    });
                })
              });
        }
    })
});

const viewAll=((req,res)=>{
    const options={
        page:1,
        limit:100,
        select : '_id first_name last_name email password user_type'
    };
    User.paginate({},options)
        .then((users) => {
            res.status(200).json({
                errors:[],
                //   data:users,
                data:users
            });
        }).catch((err) => {
        res.status(500).json({
            errors:[{message:"The data couldn't be retrieved"}],
            data:[]
        });
    });
});

const findUser=((req,res)=>{
    User.findById(req.params.id)
        .then((std)=>{
            res.status(200).json({
                errors:[],
                data:std
            });
        })
        .catch((err)=>{
            res.status(500).json({
                errors:[{message:"An error has ocurred"}],
                data:[]
            });
        });
});

const updateUser=((req,res)=>{
    User.findById(req.params.id)
        .then((std)=>{
            std.first_name= req.body.first_name ? req.body.first_name :std.first_name;
            std.last_name=req.body.last_name ? req.body.last_name : std.last_name;
            std.email= req.body.email ? req.body.email : std.email;
            std.password= req.body.password ? req.body.password : std.password;
            std.user_type = req.body.user_type ? req.body.user_type : std.user_type;


            std.save()
                .then((std)=>{
                    res.status(200).json({
                        errors:[],
                        data:std
                    });
                }).catch(()=>{
                res.status(500).json({
                    errors:[{message:"An error ocurred while updating the user"}],
                    data:[]
                });
            });
        })
        .catch((err)=>{
            res.status(500).json({
                errors:[{message:"An error ocurred while updating the user"}],
                data:[]
            });
        });
});

const deleteUser=((req,res)=>{
    User.deleteOne({_id:req.params.id})
        .then((std)=>{
            res.status(200).json({
                errors:[],
                data:std
            });
        })
        .catch((err)=>{
            res.status(500).json({
                errors:[{message:"An error ocurred while deleting the user"}],
                data:[]
            });
        });
});

const loginUser=((req, res)=>{
    User.findOne({
        $or:[
              {email:req.body.email}
        ]},(err,user)=>{
        if(err){
         return res.status(500).send({
             message: 'An error has ocurred'
            });
        }    
        if(user){
            bcrypt.compare(req.body.password, user.password,(err, areEqual)=>{
                if(err){ 
                    return res.status(500).send({
                        message: 'An error has ocurred'
                    });
                }
                if(!areEqual) 
                return res.status(404).send({
                    message: 'An error has ocurred'
                });
                let payload={subject:user._id};
                let token=jwt2.sign(payload,'secretKey');
                let role=user.user_type;
                let person=user.first_name + ' ' + user.last_name;
                res.status(200).send({
                    token,role,person
                });
            });
        }else{
            Student.findOne({
                $or:[
                      {email:req.body.email}
                ]},(err,std)=>{
                if(err){
                 return res.status(500).send({
                     message: 'An error has ocurred'
                    });
                }    
                if(std){
                    bcrypt.compare(req.body.password, std.password,(err, areEqual)=>{
                        if(err) {
                            return res.status(500).send({
                                message: 'An error has ocurred'
                            });
                        }
                        if(!areEqual) 
                        return res.status(404).send({
                            message: 'An error has ocurred'
                        });
                        let payload={subject:std._id};
                        let token=jwt2.sign(payload,'secretKey');
                        let role='Student';
                        let person=std.first_name + ' ' + std.last_name;
                        res.status(200).send({
                            token,
                            role,
                            person
                        });
                    });
                }else{
                    return res.status(404).send({
                        message: "The user doesn't exist"
                    });
                }
            });
        }
    });
});

const verify=((req,res)=>{
    let token=req.body.token;
    
    let payload = jwt2.verify(token, 'secretKey')
    if(!payload) {
      return res.status(401).send('Unauthorized request')
    }else{
        console.log(payload.subject);
        return res.status(200).send({
            message: "'Everything it's ok"
        });
    }
});

const decode=((req,res)=>{
    let token=req.body.token;
    
    let payload = jwt2.verify(token, 'secretKey')
    if(!payload) {
      return res.status(401).send('Unauthorized request')
    }else{
        let id=payload.subject;
        res.status(200).send({
            id
        });
    }

})

module.exports={
    viewAll,createUser,findUser,updateUser,deleteUser,loginUser,verify,decode
};