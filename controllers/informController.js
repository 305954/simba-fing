const Inform = require('../models/inform');
const moment = require('moment');

const createInform=((req,res,next)=>{
    let inform=new Inform({
        enrollment: req.body.enrollment,
        name: req.body.name,
        date: moment().format('Do MMMM YYYY'),
        hour: moment().format(' h:mm a')
    });

    inform.save()
        .then((inf)=>{
            res.status(200).json({
                errors:[],
                data:inf
            });
        })
        .catch((err)=>{
            res.status(500).json({
                errors:[{message:"The Inform couldn't be saved"}],
                data:[]
            });
        })
});

const viewAll=((req,res)=>{
    const options={
        page:1,
        limit:100,
        select : 'enrollment name date hour'
    };
    Inform.paginate({},options)
        .then((informs) => {
            res.status(200).json({
                errors:[],
                data:informs
            });
        }).catch((err) => {
        res.status(500).json({
            errors:[{message:"The data couldn't be retrieved"}],
            data:[]
        });
    });
});

module.exports={
    viewAll,createInform
};