const Student = require('../models/student');
const bcrypt = require('bcrypt-nodejs');

const createStudent=((req,res,next)=>{

    let student=new Student({
        first_name: req.body.first_name,
        last_name: req.body.last_name,
        enrollment: req.body.enrollment,
        email: req.body.email,
        // password: req.body.password,
        password: '12345',
        scholarship_type: req.body.scholarship_type,
        academic:req.body.academic,

        street_name:req.body.street_name,
        street2_name:req.body.street2_name,
        external_number:req.body.external_number,
        internal_number:req.body.internal_number,
        zip_code:req.body.zip_code,
        credits:req.body.credits
    });

    Student.find({
        $or:[
            {enrollment:student.enrollment}
        ]
    }).exec((err,found)=>{
        if(err){
            return res.status(500).json({
                errors:[{message:"An unexpected error has ocurred"}],
                data:[]
            });
        }
        if(found && found.length>0){
            console.log('found'+found);
            return res.status(302).json({
                errors:[{message:"The email or enrollment are already on use, please try a different one"}],
                data:[]
            });
        }else{
            bcrypt.hash(student.password, null, null, (err, hash)=>{
                student.password = hash;
                student.email = 'a'+student.enrollment+'@uach.mx';
                student.save()
                .then((std)=>{
                    res.status(200).json({
                        errors:[],
                        data:std
                    });
                })
                .catch((err)=>{
                    res.status(500).json({
                        errors:[{message:"The user couldn't be saved"}],
                        data:[]
                    });
                })
              });
        }
    })
});

const viewAll=((req,res)=>{
    const options={
        page:1,
        limit:100,
        select : '_id first_name last_name enrollment scholarship_type academic '+
        ' credits'
    };
    Student.paginate({},options)
        .then((students) => {
            // res.render('alumnos',{data:students});
          res.status(200).json({
              errors:[],
              data:students,
            // students:students
          });
        }).catch((err) => {
            console.log(err);
            res.status(500).json({
                errors:[{message:"The data couldn't be retrieved"}],
                data:[]
            });
        });
});

const findStudent=((req,res)=>{
    Student.findById(req.params.id)
    .then((std)=>{
        res.status(200).json({
            errors:[],
            data:std
        });
    })
    .catch((err)=>{
        res.status(500).json({
            errors:[{message:"An error has ocurred"}],
            data:[]
        });
    });
});

const updateStudent=((req,res)=>{
    Student.findById(req.params.id)
    .then((std)=>{
        std.first_name= req.body.first_name ? req.body.first_name :std.first_name;
        std.last_name=req.body.last_name ? req.body.last_name : std.last_name;
        std.enrollment=req.body.enrollment ? req.body.enrollment : std.enrollment;
        std.email= req.body.email ? req.body.email : std.email;
        std.password= req.body.password ? req.body.password : std.password;
        std.scholarship_type= req.body.scholarship_type ? req.body.scholarship_type : std.scholarship_type;
        std.academic= req.body.academic ? req.body.academic : std.academic;
        std.street_name= req.body.street_name ? req.body.street_name : std.street_name;
        std.street2_name= req.body.street2_name ? req.body.street2_name : std.street2_name;
        std.external_number= req.body.external_number ? req.body.external_number : std.external_number;
        std.internal_number= req.body.internal_number ? req.body.internal_number : std.internal_number;
        std.zip_code= req.body.zip_code ? req.body.zip_code : std.zip_code;
        std.credits= req.body.credits ? req.body.credits : std.credits;


        std.save()
            .then((std)=>{
                res.status(200).json({
                    errors:[],
                    data:std
                });
            }).catch(()=>{
                res.status(500).json({
                    errors:[{message:"An error ocurred while updating the user"}],
                    data:[]
                });
            });           
    })
    .catch((err)=>{
        res.status(500).json({
            errors:[{message:"An error ocurred while updating the user"}],
            data:[]
        });
    });
});

const deleteStudent=((req,res)=>{
    Student.deleteOne({_id:req.params.id})
    .then((std)=>{
        res.status(200).json({
            errors:[],
            data:std
        });
    })
    .catch((err)=>{
        res.status(500).json({
            errors:[{message:"An error ocurred while deleting the user"}],
            data:[]
        });
    });
});

const findEnrollment=((req,res)=>{
    let student=new Student({
        enrollment: req.params.enrollment
    });

    Student.find({
        $or:[
            {enrollment:student.enrollment}
        ]
    }).then((std)=>{
        res.status(200).json({
            errors:[],
            data:std
        });
    })
    .catch((err)=>{
        res.status(500).json({
            errors:[{message:"An error has ocurred"}],
            data:[]
        });
    });
});

module.exports={
    viewAll,createStudent,findStudent,updateStudent,deleteStudent, findEnrollment
};