var express = require('express');
var informController=require('../controllers/informController');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.render('corte_becas',  { title: 'SIMBA: Corte' });
});

router.post('/register',informController.createInform);
router.get('/all/:page?',informController.viewAll);


module.exports = router;