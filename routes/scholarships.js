const express = require('express');
const scholarshipController=require('../controllers/scholarshipController');
const api = express.Router();

api.get('/all/:page?',scholarshipController.viewAll);

api.get('/view/:id',scholarshipController.findScholarship);

api.post('/register',scholarshipController.createScholarship);

api.put('/update/:id',scholarshipController.updateScholarship);

api.delete('/delete/:id',scholarshipController.deleteScholarship);

api.get('/', function(req, res, next) {
    res.render('becas',  { title: 'SIMBA: Becas' });
});

module.exports = api;