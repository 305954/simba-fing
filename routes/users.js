const express = require('express');
const usersController = require('../controllers/userController');
const api=express.Router();

/* GET users listing. */
api.get('/all/:page?',usersController.viewAll);

api.get('/view/:id',usersController.findUser);

api.post('/register',usersController.createUser);

api.put('/update/:id',usersController.updateUser);

api.delete('/delete/:id',usersController.deleteUser);

api.get('/', function(req, res, next) {
    res.render('usuarios',  { title: 'SIMBA: Alumnos' });
});

module.exports = api;
