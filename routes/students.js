const express =require('express');
const studentController=require('../controllers/studentController');
const api=express.Router();

api.get('/all/:page?',studentController.viewAll);

api.get('/view/:id',studentController.findStudent);

api.post('/register',studentController.createStudent);

api.put('/update/:id',studentController.updateStudent);

api.delete('/delete/:id',studentController.deleteStudent);

api.get('/enrollment/:enrollment',studentController.findEnrollment)

api.get('/', function(req, res, next) {
    res.render('alumnos',  { title: 'SIMBA: Alumnos' });
});


module.exports=api;