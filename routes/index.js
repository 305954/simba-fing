var express = require('express');
var router = express.Router();
const usersController = require('../controllers/userController');


/* GET home page. */
router.get('/' ,function(req, res, next) {
  res.render('index', { title: 'SIMBA: Facultad de Ingeniería' });
});

router.get('/auth/login',function(req,res,next){
  res.render('login',{ title: 'SIMBA: Facultad de Ingeniería' });
});

router.post('/login',usersController.loginUser);
router.post('/verify',usersController.verify);
router.post('/decode',usersController.decode);





module.exports = router;
