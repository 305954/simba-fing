
const index = {template:'<h1> Funciona </h1>'}
const login ={template:'<h2> Funciona </h2>'}


const routes=[
    {path:'/',meta:{requiresAuth:true},name:'Index'},
    {path:'/auth/login',meta:{requiresAuth:false},name:'Login'},
    {path:'/users',meta:{requiresAuth:true},name:'Users'},
    {path:'/students',meta:{requiresAuth:true},name:'Students'},
    {path:'/charge',meta:{requiresAuth:true},name:'Charge'},
    {path:'/inform',meta:{requiresAuth:true},name:'Inform'},
    {path:'/scholarships',meta:{requiresAuth:true},name:'Scholarships'},
]

const router=new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
  let url= (window.location.pathname);
    // if(to.matched.some(record => record.meta.requiresAuth)) {
      if (localStorage.getItem('token') == null && url!='/auth/login') {
        next(
           (window.location=('/auth/login'))
        );
      }
    // }
})

const app = new Vue({
    el:'#app',
    router,
    data:{
      //Objetos que contengan informacion de la app (modelos)
      students:[],
      student:[],
      studentId:'',
      users:[],
      user:[],
      userId:'',
      enrollment:'',
      studentpayment:[],
      scholarships:[],
      scholarship:[],
      scholarshipId:'',
      scholarshipTitle:'',
      edScholarshipTitle:'',
      loginData:[],
      name:localStorage.getItem('name'),
      role:localStorage.getItem('role'),
      id:'',
      informs:[],
      flag:0
    },
    methods:{
      // Students
      getStudent(id){
        fetch("/students/view/"+id)
        .then((response) => {return response.json()})
        .then((data) =>{
          this.student=data;
          console.log(this.student);
          document.getElementById("edNombre").value=new String(this.student.data.first_name);
          let last_name1=new String(this.student.data.last_name);
          let apellidos=last_name1.split(' ');
          document.getElementById("edApellido").value=new String(apellidos[0]);
          document.getElementById("edApellido2").value=new String(apellidos[1]);
          document.getElementById("edMatricula").value=new String(this.student.data.enrollment);
          document.getElementById("edCarrera").value=new String(this.student.data.academic);

          this.scholarships.forEach(schol=>{
            if(schol._id===this.student.data.scholarship_type){
              console.log(schol.title);
              document.getElementById("edScholarship_type").value=new String(schol.title);
            }
          });

          
        });
      },
      updateStudent(){
        id=this.student.data._id;
        first_name=document.getElementById("edNombre").value;
        last_name=document.getElementById("edApellido").value + ' '+ document.getElementById("edApellido2").value;
        enrollment=document.getElementById("edMatricula").value;
        academic=document.getElementById("edCarrera").value;
        // scholarship_type=document.getElementById("edBeca").value;
        
        this.scholarships.forEach(schol=>{
          if(schol.title===this.edScholarshipTitle){
            scholarship_type=schol._id;
            credits=schol.credits;
          }
        });
        
        const datos ={
          id:id,
          first_name:first_name,
          last_name:last_name,
          enrollment:enrollment,
          academic:academic,
          scholarship_type:scholarship_type,
          credits:credits,
        };
        const options={
          method:'PUT',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch("/students/update/"+id,options)
        .then(response => response.json())
        .then(json => {
          this.students = json.data.docs;
        });
        (window.location="/students")
      },
      addStudent(){
        first_name=document.getElementById("first_name").value;
        last_name=document.getElementById("last_name").value + ' '+ document.getElementById("last_name2").value;
        enrollment=document.getElementById("enrollment").value;
        academic=document.getElementById("academic").value;
        // scholarship_type=document.getElementById("scholarship_type").value;
        this.scholarships.forEach(schol=>{
          if(schol.title===this.scholarshipTitle){
            scholarship_type=schol._id;
            credits=schol.credits;
          }
        });
        
        const datos ={
          first_name:first_name,
          last_name:last_name,
          enrollment:enrollment,
          academic:academic,
          scholarship_type:scholarship_type,
          credits:credits,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch("/students/register",options)
        .then(response => response.json())
        .then(json => {
          this.students = json.data.docs;
        });
         (window.location="/students")
      },
      getId(id){
        this.studentId=id;
      },
      deleteStudent(){
        const options={
          method:'DELETE',
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch("/students/delete/"+this.studentId,options)
        .then(response => response.json())
        .then(json => {
          this.users = json.data.docs;
        });
        (window.location="/students")
      },
      //Users
      addUser(){
        first_name=document.getElementById("first_name").value;
        last_name=document.getElementById("last_name").value + ' '+ document.getElementById("last_name2").value;
        email=document.getElementById("email").value;
        user_type=document.getElementById("user_type").value;
        const datos ={
          first_name:first_name,
          last_name:last_name,
          email:email,
          user_type:user_type,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch("/users/register",options)
        .then(response => response.json())
        .then(json => {
          this.users = json.data.docs;
        });
         (window.location="/users")



      },
      getUserId(id){
        this.userId=id;
      },
      deleteUser(){
        const options={
          method:'DELETE',
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch("/users/delete/"+this.userId,options)
        .then(response => response.json())
        .then(json => {
          this.users = json.data.docs;
        });
        (window.location="/users")
      },
      getUser(id){
        fetch("/users/view/"+id)
        .then((response) => {return response.json()})
        .then((data) =>{
          this.user=data;
          document.getElementById("edfirst_name").value=new String(this.user.data.first_name);
          let last_name1=new String(this.user.data.last_name);
          let apellidos=last_name1.split(' ');
          document.getElementById("edlast_name").value=new String(apellidos[0]);
          if(apellidos[1]==undefined){
            document.getElementById("edlast_name2").value=new String('');
          }else{
            document.getElementById("edlast_name2").value=new String(apellidos[1]);
          }
          
          document.getElementById("edemail").value=new String(this.user.data.email);
          document.getElementById("eduser_type").value=new String(this.user.data.user_type);
        });
      },
      updateUser(){
        id=this.user.data._id;
        first_name=document.getElementById("edfirst_name").value;
        last_name=document.getElementById("edlast_name").value + ' '+ document.getElementById("edlast_name2").value;
        email=document.getElementById("edemail").value;
        user_type=document.getElementById("eduser_type").value;
        const datos ={
          id:id,
          first_name:first_name,
          last_name:last_name,
          email:email,
          user_type:user_type,
        };
        const options={
          method:'PUT',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };
        console.log(datos);
        fetch("/users/update/"+id,options)
        .then(response => response.json())
        .then(json => {
          this.users = json.data.docs;
        });
        (window.location="/users")
      },
      //Schoolarships
      getScholarship(id){
        fetch("/scholarships/view/"+id)
        .then((response) => {return response.json()})
        .then((data) =>{
          this.scholarship=data;
          console.log(this.scholarship);
          document.getElementById("edKey").value=new String(this.scholarship.data.key);
          document.getElementById("edTitle").value=new String(this.scholarship.data.title);
          document.getElementById("edDescription").value=new String(this.scholarship.data.description);
          document.getElementById("edCredits").value=new String(this.scholarship.data.credits);
        });
      },
      updateScholarship(){
        id=this.scholarship.data._id;
        key=document.getElementById("edKey").value;
        title=document.getElementById("edTitle").value
        description=document.getElementById("edDescription").value;
        credits=document.getElementById("edCredits").value;
        const datos ={
          id:id,
          key:key,
          title:title,
          description:description,
          credits:credits,
        };
        const options={
          method:'PUT',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch("/scholarships/update/"+id,options)
        .then(response => response.json())
        .then(json => {
          this.students = json.data.docs;
        });
        (window.location="/scholarships")
      },
      addScholarship(){
        key=document.getElementById("key").value;
        title=document.getElementById("title").value
        description=document.getElementById("description").value;
        credits=document.getElementById("credits").value;
        const datos ={
          key:key,
          title:title,
          description:description,
          credits:credits,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch("/scholarships/register",options)
        .then(response => response.json())
        .then(json => {
          this.students = json.data.docs;
        });
         (window.location="/scholarships")
      },
      getScholarshipId(id){
        this.scholarshipId=id;
      },
      deleteScholarship(){
        const options={
          method:'DELETE',
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch("/scholarships/delete/"+this.scholarshipId,options)
        .then(response => response.json())
        .then(json => {
          this.users = json.data.docs;
        });
        (window.location="/scholarships")
      },
      //Payment
      getStudentByEnrollment(){
        document.getElementById("first_name").value=new String('');
        document.getElementById("enroll").value=new String('');
        document.getElementById("enrollment").value=new String('');
        document.getElementById("academic").value=new String('');
        document.getElementById("days").value=new String('');
        fetch("/students/enrollment/"+this.enrollment)
        .then((response) => {return response.json()})
        .then((data) =>{
          if(data.data.length==0){
            alert('Lo sentimos, la matricula que ingreso es incorrecta o no tiene beca alimenticia en este momento');
          }else{
            this.studentpayment=data;
            document.getElementById("first_name").value=
            new String(this.studentpayment.data[0].first_name) + ' ' + new String(this.studentpayment.data[0].last_name);
            document.getElementById("enroll").value=new String(this.studentpayment.data[0].enrollment);
            document.getElementById("academic").value=new String(this.studentpayment.data[0].academic);
            document.getElementById("days").value=new String(this.studentpayment.data[0].credits);
          }
        });
      },
      charge(){
        credits=parseInt(this.studentpayment.data[0].credits)-1;
        const datos ={
          credits:credits,
        };
        const options={
          method:'PUT',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch("/students/update/"+this.studentpayment.data[0]._id,options)
        .then(response => response.json())
        .then(json => {
          this.students = json.data.docs;
        });
        (window.location="/charge")
      },
      // Login/Logout
      login(){
        email=document.getElementById("email").value;
        password=document.getElementById("password").value;
        const datos ={
          email:email,
          password:password,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch('/login',options)
        .then(response=>response.json())
        .then(json=>{
          this.loginData.token=json.token;
          this.loginData.person=json.person;
          this.loginData.role=json.role;
          localStorage.removeItem('token');
          localStorage.setItem('token',this.loginData.token);
          localStorage.setItem('name',json.person);
          localStorage.setItem('role',json.role);
          // (window.location="/")

          let token=localStorage.getItem('token');
          const datos2 ={
            token:token,
          };
          const options2={
            method:'POST',
            body:JSON.stringify(datos2),
            headers:{
              'Content-Type': 'application/json'
            }
          };     
          fetch('/verify',options2)
          .then(response=>{
            if(response.ok){
              (window.location="/")
            }else{
              this.logout();
            }
          });


        });

      },
      logout(){
        localStorage.removeItem('token');
        localStorage.removeItem('role');
        localStorage.removeItem('name');
        (window.location="/auth/login");
      },
      //URL
      indexUrl(){
        let token=localStorage.getItem('token');
        const datos ={
          token:token,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };     
        fetch('/verify',options)
        .then(response=>{
          if(response.ok){
            (window.location="/")
          }else{
            this.logout();
          }
        });
      },
      chargeUrl(){
        let token=localStorage.getItem('token');
        const datos ={
          token:token,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };     
        fetch('/verify',options)
        .then(response=>{
          if(response.ok){
            (window.location="/charge")
          }else{
            this.logout();
          }
        });
      },
      studentsUrl(){
        let token=localStorage.getItem('token');
        const datos ={
          token:token,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };     
        fetch('/verify',options)
        .then(response=>{
          if(response.ok){
            (window.location="/students")
          }else{
            this.logout();
          }
        });
      },
      informUrl(){
        let token=localStorage.getItem('token');
        const datos ={
          token:token,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };     
        fetch('/verify',options)
        .then(response=>{
          if(response.ok){
            (window.location="/inform")
          }else{
            this.logout();
          }
        });
      },
      scholarshipsUrl(){
        let token=localStorage.getItem('token');
        const datos ={
          token:token,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };     
        fetch('/verify',options)
        .then(response=>{
          if(response.ok){
            (window.location="/scholarships")
          }else{
            this.logout();
          }
        });
      },
      usersUrl(){
        let token=localStorage.getItem('token');
        const datos ={
          token:token,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };     
        fetch('/verify',options)
        .then(response=>{
          if(response.ok){
            (window.location="/users")
          }else{
            this.logout();
          }
        });
      },
      //Inform
      addInform(){
        name=document.getElementById('first_name').value;
        enrollment=document.getElementById('enroll').value;

        const datos ={
          name:name,
          enrollment:enrollment,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };
        fetch("/inform/register",options)
        .then(response => response.json())
        .then(json => {
          this.informs = json.data.docs;
          this.charge()
        });
      },
      verify(){
        let token=localStorage.getItem('token');
        const datos ={
          token:token,
        };
        const options={
          method:'POST',
          body:JSON.stringify(datos),
          headers:{
            'Content-Type': 'application/json'
          }
        };     
        fetch('/verify',options)
        .then(response=>{
          if(response.ok){
            (window.location="/")
          }else if(this.flag==0){
            this.flag++;
            (window.location="/auth/login")
          }
        });
      }
      
    },
    computed:{
      // Funciones que podran ser desplegables en las vistas
      // solo si regresan un resultado
      result(){
      }
    },
    created(){
      // Aqui se ejecuta codigo al inicializar la aplicacion, como si fuera el constructor
      fetch('/users/all')
      .then(response=>response.json())
      .then(json=>{
          this.users=json.data.docs;
      });
      fetch('/scholarships/all')
      .then(response=>response.json())
      .then(json=>{
          this.scholarships=json.data.docs;
      });
      fetch('/students/all')
      .then(response=>response.json())
      .then(json=>{
          this.students=json.data.docs;
          this.students.forEach(std=>{
            this.scholarships.forEach(schol=>{
              if(std.scholarship_type===schol._id){
                std.scholarship_type=schol.title;
              }
            });
          });
      });
      fetch('/inform/all')
      .then(response=>response.json())
      .then(json=>{
          this.informs=json.data.docs;
      });
      let token=localStorage.getItem('token');
      const datos ={
        token:token,
      };
      const options={
        method:'POST',
        body:JSON.stringify(datos),
        headers:{
          'Content-Type': 'application/json'
        }
      };     
      fetch('/decode',options)
      .then(response=>response.json())
      .then(json=>{
        this.id=json.id
      });
    }
  }).$mount('#app');;