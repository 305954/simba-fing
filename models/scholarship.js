const mongoose=require('mongoose');
const schema=mongoose.Schema;
const mongoosePaginate=require('mongoose-paginate-v2');

const ScholarshipSchema = new schema({
    key: String,
    title: String,
    description: String,
    credits: Number
});

ScholarshipSchema.plugin(mongoosePaginate);
module.exports=mongoose.model('Scholarship', ScholarshipSchema);