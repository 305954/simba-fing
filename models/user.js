const mongoose=require('mongoose');
const schema=mongoose.Schema;
const mongoosePaginate=require('mongoose-paginate-v2');

const UserSchema = new schema({
    first_name: String,
    last_name: String,
    email: String,
    password: String,
    user_type: String
});

UserSchema.plugin(mongoosePaginate);
module.exports=mongoose.model('User',UserSchema);