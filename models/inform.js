const mongoose=require('mongoose');
const schema=mongoose.Schema;
const mongoosePaginate=require('mongoose-paginate-v2');

const InformSchema = new schema({
    enrollment: String,
    name: String,
    date: String,
    hour: String
});

InformSchema.plugin(mongoosePaginate);
module.exports=mongoose.model('Inform',InformSchema);