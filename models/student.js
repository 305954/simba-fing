const mongoose=require('mongoose');
const schema=mongoose.Schema;
const mongoosePaginate=require('mongoose-paginate-v2');

const StudentSchema = new schema({
    first_name: String,
    last_name: String,
    enrollment: String,
    email: String,
    password: String,
    scholarship_type: {type: schema.ObjectId, ref: 'Scholarship'},
    academic:String,
    street_name:String,
    street2_name:String,
    external_number:Number,
    internal_number:Number,
    zip_code:Number,
    credits:Number
});

StudentSchema.plugin(mongoosePaginate);
module.exports=mongoose.model('Student',StudentSchema);